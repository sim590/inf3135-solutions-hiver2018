# INF3135 - Solutions

Ce dépôt contient les références vers les solutions de tous les labos sous-forme
de sous-modules git.

# Sur Gitlab

Il n'y a qu'à cliquer sur la solution qu'on désire visiter.

# Sur sa propre machine

Vous devez initialiser les sous-modules comme suit:

```sh
$ git submodule update --init
```

Ensuite, toutes les solutions seront initialisées dans les sous-répertoires de
ce dépôt.
